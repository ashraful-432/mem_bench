# include <stdio.h>
# include <math.h>
# include <float.h>
# include <limits.h>
# include <sys/time.h>
#include <stdlib.h>

#ifndef N
#   define N	2000000
#endif
#ifndef NTIMES
#   define NTIMES	500
#endif
#ifndef OFFSET
#   define OFFSET	0
#endif

# define HLINE "-------------------------------------------------------------\n"

# ifndef MIN
# define MIN(x,y) ((x)<(y)?(x):(y))
# endif
# ifndef MAX
# define MAX(x,y) ((x)>(y)?(x):(y))
# endif

#ifndef L1
#define L1 (32*1024)/8
#endif

#ifndef L2
#define L2 (128*1024)/8
#endif

#ifndef L3
#define L3 (3024*1024)/8
#endif


#ifndef QUANTA
#define QUANTA 128
#endif

/*
static double	a[N+OFFSET];
static double	b[N+OFFSET];
static double	c[N+OFFSET];
*/
static double	bytes[4] = {
    2 * sizeof(double) * N,
    2 * sizeof(double) * N,
    3 * sizeof(double) * N,
    3 * sizeof(double) * N
    };

static double	avgtime[4] = {0};
static double	maxtime[4] = {0};
static double	mintime[4] = {FLT_MAX,FLT_MAX,FLT_MAX,FLT_MAX};

static char	*label[4] = {"Copy:      ", "Scale:     ",
    "Add:       ", "Triad:     "};

extern double mysecond();
extern void checkSTREAMresults();
extern int checktick();

int main()
{
    int			quantum;
    int			BytesPerWord;
    register int	j, k, iTIMES, quanta_count;
    double		scalar, t;
    double		times[4];
    double		*a;
    double		*b;
    double		*c;

    FILE *fp_copy;
    FILE *fp_scale;
    FILE *fp_add;
    FILE *fp_triad;

    FILE *fp_copybw;
    FILE *fp_scalebw;
    FILE *fp_addbw;
    FILE *fp_triadbw;

    fp_copy = fopen("./copy.rpt","w");
    if(fp_copy==NULL)
    {
      printf("Error opening file 'copy.rpt'\n");
      return(-1);
    }

    fp_scale = fopen("./scale.rpt","w");
    if(fp_scale==NULL)
    {
      printf("Error opening file 'scale.rpt'\n");
      return(-1);
    }

    fp_add = fopen("./add.rpt","w");
    if(fp_add==NULL)
    {
      printf("Error opening file 'add.rpt'\n");
      return(-1);
    }

    fp_triad = fopen("./triad.rpt","w");
    if(fp_triad==NULL)
    {
      printf("Error opening file 'triad.rpt'\n");
      return(-1);
    }

    fp_copybw = fopen("./copy_bw.rpt","w");
    if(fp_copy==NULL)
    {
      printf("Error opening file 'copy_bw.rpt'\n");
      return(-1);
    }

    fp_scalebw = fopen("./scale_bw.rpt","w");
    if(fp_scale==NULL)
    {
      printf("Error opening file 'scale.rpt'\n");
      return(-1);
    }

    fp_addbw = fopen("./add_bw.rpt","w");
    if(fp_add==NULL)
    {
      printf("Error opening file 'add_bw.rpt'\n");
      return(-1);
    }

    fp_triadbw = fopen("./triad_bw.rpt","w");
    if(fp_triad==NULL)
    {
      printf("Error opening file 'triad_bw.rpt'\n");
      return(-1);
    }


    printf(HLINE);
    BytesPerWord = sizeof(double);
    printf("Starting Benchmark with %d bytes per DOUBLE PRECISION word.\n",
	BytesPerWord);
    printf("L1 Cache Size = %d double precision floating-point numbers\n", L1);
    printf("L2 Cache Size = %d double precision floating-point numbers\n", L2);
    printf("L3 Cache Size = %d double precision floating-point numbers\n", L3);

    //printf("Each test will run %d times\n", NTIMES);
    printf(HLINE);
    quantum = QUANTA;
    quanta_count=0;
    while(quantum<L3)
    {
        if(!(a = malloc(sizeof(double)*quantum)))
        {
          printf("malloc failed for 'a' at size:%d \n",quantum);
          return(-1);
        }
        if(!(b = malloc(sizeof(double)*quantum)))
        {
          printf("malloc failed for 'b' at size:%d \n",quantum);
          return(-1);
        }
        if(!(c = malloc(sizeof(double)*quantum)))
        {
          printf("malloc failed for 'c' at size:%d \n",quantum);
          return(-1);
        }

        iTIMES = 500;//checktick(quantum);
	//printf("Got the iTIMES = %d\n",iTIMES);

        /*	--- MAIN LOOP --- repeat test cases iTIMES times --- */
        for (j=0; j<quantum; j++)
        {
          a[j] = (float)(j);
          b[j] = (float)(j+1);
          c[j] = 0.0;
        }

        scalar = 3.0;

        // 1: Copy operation
        times[0] = mysecond();
        for (k=0; k<iTIMES; k++)
        {
          if(k==1)
            times[0] = mysecond();
          //printf("copy operation\n");
          for (j=0; j<quantum; j++)
          {
            c[j] = a[j];
          }
        }
        times[0] = mysecond() - times[0];

        // 2: Scale operation
        times[1] = mysecond();
        //printf("scale operation\n");
        for (k=0; k<iTIMES; k++)
        {
          if(k==1)
            times[1] = mysecond();
          for (j=0; j<quantum; j++)
          {
            b[j] = scalar*c[j];
          }
        }
        times[1] = mysecond() - times[1];

        // 3: Add operation
        times[2] = mysecond();
        for (k=0; k<iTIMES; k++)
        {
          if(k==1)
            times[2] = mysecond();
          for (j=0; j<quantum; j++)
          {
            c[j] = a[j]+b[j];
          }
        }
	      times[2] = mysecond() - times[2];

        // 4: Triad operation
        times[3] = mysecond();
        for (k=0; k<iTIMES; k++)
        {
          if(k==1)
            times[3] = mysecond();
          for (j=0; j<quantum; j++)
          {
            a[j] = b[j]+scalar*c[j];
          }
        }
        times[3] = mysecond() - times[3];

        /*	--- SUMMARY --- */
        for (j=0; j<4; j++)
        {
          avgtime[j] = times[j];
		      //mintime[j] = MIN(mintime[j], times[j][k]);
		      //maxtime[j] = MAX(maxtime[j], times[j][k]);
        }

        //printf("Function      Rate (MB/s)   Avg time     Min time     Max time\n");
        //printf("Function   Avg time for %d Byte\n",quantum*8);
        for (j=0; j<4; j++)
        {
          avgtime[j] = avgtime[j]/(double)(iTIMES-1);
          if(j==0)
          {
            fprintf(fp_copy, "%d %5.4f\n", quantum, avgtime[j]*1E6);
            fprintf(fp_copybw, "%d %.4f\n", quantum, ((quantum*8) / avgtime[j])/1E6);
          }
          if(j==1)
          {
            fprintf(fp_scale, "%d %5.4f\n", quantum, avgtime[j]*1E6);
            fprintf(fp_scalebw, "%d %.4f\n", quantum, ((quantum*8) / avgtime[j])/1E6);
          }
          if(j==2)
          {
            fprintf(fp_add, "%d %5.4f\n", quantum, avgtime[j]*1E6);
            fprintf(fp_addbw, "%d %.4f\n", quantum, ((quantum*8) / avgtime[j])/1E6);
          }
          if(j==3)
          {
            fprintf(fp_triad, "%d %5.4f\n", quantum, avgtime[j]*1E6);
            fprintf(fp_triadbw, "%d %5.4f\n", quantum, ((quantum*8) / avgtime[j])/1E6);
          }
          //printf("%s %f  \n", label[j], avgtime[j]*1E6);
          avgtime[j] = 0.0;
        }
        
        //printf(HLINE);

        free(a);
        free(b);
        free(c);
        if(quantum>L2)
        {
          quantum = quantum + (4*QUANTA);
        }
        else if(quantum>L1)
        {
          quantum = quantum + (2*QUANTA);
        }
        else
        {
          quantum = quantum + (QUANTA);
        }
        quanta_count++;
    }

    fclose(fp_copy);
    fclose(fp_scale);
    fclose(fp_add);
    fclose(fp_triad);
    fclose(fp_copybw);
    fclose(fp_scalebw);
    fclose(fp_addbw);
    fclose(fp_triadbw);

    /* --- Check Results --- */
//    checkSTREAMresults();
    printf(HLINE);

    return 0;
}


# define	M	20

int checktick(int varN)
{
    int		i, minDelta, Delta;
    double	t1, t2, timesfound[M];
    int j, x, y;
    double *ta;
//    varN = 32;
    if( !(ta = malloc(sizeof(double)*varN))) printf("malloc failed\n");

/*  Collect a sequence of M unique time values from the system. */
    x = 100;
    for (i = 0; i < M; i++) {
        loop:
	t1 = mysecond();
	for(j=0; j<x; j++)
        {
             for(y=0; y<varN; y++)
                  ta[y] = (float) (y);
        }
	t2 = mysecond();
	timesfound[i] = t2 - t1;
        if(fabs(timesfound[i])<20.0E-6)
        {
		x = x + 100;
		goto loop;
        }
    }

#if 0
    printf("Suggested X = %d\n",x);
    for (i = 0; i < M; i++) {
        printf("timesfound[%d] = %f\n",i,timesfound[i]);
    }
#endif

   return(x);
    }

/* A gettimeofday routine to give access to the wall
   clock timer on most UNIX-like systems.  */

#include <sys/time.h>

double mysecond()
{
        struct timeval tp;
        struct timezone tzp;
        int i;

        i = gettimeofday(&tp,&tzp);
        return ( (double) tp.tv_sec + (double) tp.tv_usec * 1.e-6 );
}
#if 0
void checkSTREAMresults ()
{
	double aj,bj,cj,scalar;
	double asum,bsum,csum;
	double epsilon;
	int	j,k;

    /* reproduce initialization */
	aj = 1.0;
	bj = 2.0;
	cj = 0.0;
    /* a[] is modified during timing check */
	aj = 2.0E0 * aj;
    /* now execute timing loop */
	scalar = 3.0;
	for (k=0; k<NTIMES; k++)
        {
            cj = aj;
            bj = scalar*cj;
            cj = aj+bj;
            aj = bj+scalar*cj;
        }
	aj = aj * (double) (N);
	bj = bj * (double) (N);
	cj = cj * (double) (N);

	asum = 0.0;
	bsum = 0.0;
	csum = 0.0;
	for (j=0; j<N; j++) {
		asum += a[j];
		bsum += b[j];
		csum += c[j];
	}
#ifdef VERBOSE
	printf ("Results Comparison: \n");
	printf ("        Expected  : %f %f %f \n",aj,bj,cj);
	printf ("        Observed  : %f %f %f \n",asum,bsum,csum);
#endif

#ifndef abs
#define abs(a) ((a) >= 0 ? (a) : -(a))
#endif
	epsilon = 1.e-8;

	if (abs(aj-asum)/asum > epsilon) {
		printf ("Failed Validation on array a[]\n");
		printf ("        Expected  : %f \n",aj);
		printf ("        Observed  : %f \n",asum);
	}
	else if (abs(bj-bsum)/bsum > epsilon) {
		printf ("Failed Validation on array b[]\n");
		printf ("        Expected  : %f \n",bj);
		printf ("        Observed  : %f \n",bsum);
	}
	else if (abs(cj-csum)/csum > epsilon) {
		printf ("Failed Validation on array c[]\n");
		printf ("        Expected  : %f \n",cj);
		printf ("        Observed  : %f \n",csum);
	}
	else {
		printf ("Solution Validates\n");
	}
}
#endif
