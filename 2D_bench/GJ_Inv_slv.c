//LUsolve.cpp : Defines the entry point for the console application.
//

//#include "stdafx.h"
#include "stdio.h"
#include "stdlib.h"
#include "math.h"

#define DP
#define IDX2C(i,j,ld) (((j)*(ld))+(i))

#ifdef SP
#define REAL float
#define ZERO 0.0
#define ONE 1.0
#define PREC "Single "
#endif

#ifdef DP
#define REAL double
#define ZERO 0.0e0
#define ONE 1.0e0
#define PREC "Double "
#endif

#define NO_PARTIAL_PIVOT
#define MSIZE 500
//#define DEBUG

//void dgefa(REAL a[], int lda, int n, int ipvt[], int *info);
//void dgesl(REAL a[], int lda, int n, int ipvt[], REAL b[], int job);
//int idamax(int n, REAL dx[], int incx);
#ifndef NO_PARTIAL_PIVOT
static int dfwd_sbs(REAL a[], int lda, int n, int ipvt[], REAL b[], int *info);
static int idamax(int n, REAL dx[]);
static int dgesl(REAL a[], int lda, int n, int ipvt[], REAL b[]);
#else
static int dfwd_sbs(REAL a[], int lda, int n, REAL b[], int *info);
static int idamax(int n, REAL dx[]);
static int dgesl(REAL a[], int lda, int n, REAL b[]);
#endif


int GJ_Inv_slv(int lda, int n, REAL *a, REAL *b)
{
	//REAL *c;
#ifndef NO_PARTIAL_PIVOT
	int *ipvt;
#endif
	int info;
	int status;
	int i, j;

/*	if (!(c = (REAL *) malloc(lda * n * sizeof(REAL))))
	{
		printf("GJ_Inv_slv: Malloc fails for c[].");
		exit (-1);
	}
*/
#ifndef NO_PARTIAL_PIVOT
		if (!(ipvt = (int *)malloc(lda * sizeof(int))))
		{
			printf("Malloc fails for ipvt[].");
			exit (-3);
		}
#endif //NO_PARTIAL_PIVOT
/*
	for ( j = 0; j < n; j++)
	{
		for ( i = 0; i < lda; i++)
		{
			c[IDX2C(i,j,lda)] = a[IDX2C(i,j,lda)];
		}
	}
*/
	//----------------------
	// Forward Substitution
	//----------------------
	status = dfwd_sbs(a, lda, n,
#ifndef NO_PARTIAL_PIVOT
		ipvt,
#endif
		b, &info);
	//----------------------
	// Backward Substitution
	//----------------------
	dgesl(a, lda, n,
#ifndef NO_PARTIAL_PIVOT
		ipvt,
#endif
		b);
#ifdef DEBUG
		double e=0;
		for (i = 0; i < lda; i++)
		{
			e = e + (b[i] - 1.0);
		}
		printf("error e = %f \n", e);
#endif
    return 0;
}

int idamax(int n, REAL dx[])//, int incx)
{
	//	finds the index of element having max. absolute value.
	// Note that the matrix is column major format
	REAL dmax;
	int i, ix, itemp;

	if (n < 1) return(-4);
	if (n == 1) return(0);

	itemp = 0;
	dmax = fabs((double)dx[0]);
	for (i = 1; i < n; i++) {
		if (fabs((double)dx[i]) > dmax)
		{
			itemp = i;
			dmax = fabs((double)dx[i]);
		}
	}
	return (itemp);
}

int dfwd_sbs(REAL a[], int lda, int n,
#ifndef NO_PARTIAL_PIVOT
	int ipvt[],
#endif
	REAL b[], int *info)
{
	/* dgefa factors a double precision matrix by gaussian elimination.

	on entry

	a       REAL precision[n][lda]
	the matrix to be factored.

	lda     integer
	the leading dimension of the array  a .

	n       integer
	the order of the matrix  a .

	on return

	a       an upper triangular matrix and the multipliers
	which were used to obtain it.
	the factorization can be written  a = l*u  where
	l  is a product of permutation and unit lower
	triangular matrices and  u  is upper triangular.

	ipvt    integer[n]
	an integer vector of pivot indices.

	info    integer
	= 0  normal value.
	= k  if  u[k][k] .eq. 0.0 .  this is not an error
	condition for this subroutine, but it does
	indicate that dgesl or dgedi will divide by zero
	if called.  use  rcond  in dgeco for a reliable
	indication of singularity.*/

	REAL t;
	int j, k, kp1, l, nm1;
	int cx, cy;

	/*     gaussian elimination with partial pivoting       */

	*info = 0;
	nm1 = n - 1;

	if (nm1 < 1) return(-4);
	if (nm1 == 1) return(0);

	for (k = 0; k < n; k++)
	{
			kp1 = k + 1;

			// find l = partial pivot index
			// if present diagonal element is
			// largest value compared to the
			// lower rows in the same column
			// then it will return the value of 0
			//l = idamax(n - k, &a[lda*k + k], 1) + k;
#ifdef NO_PARTIAL_PIVOT
			l = k;
#else
			l = idamax(n - k, &a[lda*k + k]) + k;
			ipvt[k] = l;
#endif
			// Check does partial pivot find any
			// non zero value into diagonal element
			if (a[lda*k + l] != ZERO)
			{
#ifndef NO_PARTIAL_PIVOT
				/* interchange if necessary */
				if (l != k)
				{
					//Apply row swapping as per pivot index
					for (cx = k; cx < n; cx++)
					{
						t = a[lda*cx + l]; //t holds values of row[l]col[cx]
						a[lda*cx + l] = a[lda*cx + k]; //row[l]col[cx] = row[k]col[cx]
						a[lda*cx + k] = t; //row[k]col[cx] = t
					}
					//similarly swap b
					t = b[l];
					b[l] = b[k];
					b[k] = t;
				}
#endif
				/* compute multipliers */
				t = ONE / a[lda*k + k];
				//dscal(n - (k + 1), t, &a[lda*k + k + 1], 1);
				b[k] = b[k] * t; //scale b[k]
				for (cx = k; cx < n; cx++)
				{
					a[lda*cx + k] = t * a[lda*cx + k]; // scale row k
				}
				/* row elimination with column indexing */
				for (j = kp1; j < n; j++)
				{
					//for all rows below k i.e. k+1
					t = (-1.0) * a[lda*k + j]; //treat same column k
					b[j] = b[j] + (t * b[k]);
					for (cx = k; cx < n; cx++)
					{
						//for all columnn starting from k of given row
						a[lda*cx + j] = a[lda*cx + j] + t * a[lda*cx + k];
					}
				}
			}
			else
			{
				*info = k;
			}
	}
	return (0);
}

int dgesl(REAL a[], int lda, int n,
#ifndef NO_PARTIAL_PIVOT
	int ipvt[],
#endif
	REAL b[])
{
	/*
	dgesl solves the double precision system
	a * x = b  or  trans(a) * x = b
	using the factors computed by dgeco or dgefa.

	on entry

	a       double precision[n][lda]
	the output from dgeco or dgefa.

	lda     integer
	the leading dimension of the array  a .

	n       integer
	the order of the matrix  a .

	ipvt    integer[n]
	the pivot vector from dgeco or dgefa.

	b       double precision[n]
	the right hand side vector.
	on return

	b       the solution vector  x .

	error condition

	a division by zero will occur if the input factor contains a
	zero on the diagonal.  technically this indicates singularity
	but it is often caused by improper arguments or improper
	setting of lda .  it will not occur if the subroutines are
	called correctly a*/
	REAL t;
	int k, kb, l, nm1;

	nm1 = n - 1;

	/* now solve  u*x = y */
	for (kb = 0; kb < n; kb++)
	{
		//printf("dbg_dgesl::kb=%d ", kb);
		k = n - (kb + 1);
		//printf("k = %d\n", k);
		b[k] = b[k] / a[lda*k + k];
		t = -b[k];
		//daxpy(k, t, &a[lda*k + 0], 1, &b[0], 1);
		for (l = 0; l < k; l++)
		{
			//printf("dbg_dgesl::l=%d ", l);
			b[l] = b[l] + t * a[(lda*k)+l];
		}
		//printf("\n");
	}
	return 0;
}
