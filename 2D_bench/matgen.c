#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <math.h>
#include "matgen.h"
//#define IDX2C(i,j,ld) (( i )+(( j )*( ld ))) //i:row, j:column, lda:leading dimension

//------------------------------------------------------------------------------
double mysecond()
{
        struct timeval tp;
        struct timezone tzp;
        int i;

        i = gettimeofday(&tp,&tzp);
        return ( (double) tp.tv_sec + (double) tp.tv_usec * 1.e-6 );
}
//------------------------------------------------------------------------------
void matgen(double *a, int lda, int m, int n, double *b, _mat_type_ mat_type, _offdiag_sign_ offdiag_sign)
{
// Generate test Matrix A and B for A.X = B
// Where X is unit vector (i.e. all 1)
// A, B and X matrices are stored as
// Column major format
//
// lda - is leading dimension of A and B
//       which is total number of Row
// n   - is total number of column of A
//       B's number of column is 1
// So index of the matrix A for row i & col j will be
// index = (lda * j) + i
// First generate the data
// e.g. 4x4 matrix
// 1 2 3 4
// 2 3 4 5
// 3 4 5 6
// 4 5 6 7
// if NEGATE_OFF_DIAG is defined Then
//  1 -2 -3 -4
// -2  3 -4 -5
// -3 -4  5 -6
// -4 -5 -6  7
// if BIG_DIAG is defined
// Then Multiply them in such that
// a[x: ] = a[x: ] * (row_ind + col_ind)
// and then swap the diag elemnts
// e.g.
// 12  2  6  1
//  2 20 12  6
//  6 12 30 20
// 12 20 30 42
// if NEGATE_OFF_DIAG & BIG_DIAG is defined Then
// 12    -2   -6   -1
// -2    20  -12   -6
// -6   -12   30  -20
// -12  -20  -30   42

	int i, j;
	//int m = lda;

	//Generate A matrix
	for (j = 0; j<n; j++) //Columnwise
	{
		for (i = 0; i <m; i++) //scan all rows in that column
		{
      //#ifdef TRIMATB_LOWER_UNIT
      if(mat_type==LOWER_TRIMAT_UNIT_DIAG)
      {
        if(i==j)
          a[IDX2C(i,j,lda)] = 1.0;
        else if (i>j)
          a[IDX2C(i,j,lda)] = (double)(j + i + 1);
        else
          a[IDX2C(i,j,lda)] = 0.0;
      }
      //#ifdef TRIMATB_UPPER_UNIT
      else if(mat_type==UPPER_TRIMAT_UNIT_DIAG)
      {
        if(i==j)
          a[IDX2C(i,j,lda)] = 1.0;
        else if (i<j)
          a[IDX2C(i,j,lda)] = (double)(j + i + 1);
        else
          a[IDX2C(i,j,lda)] = 0.0;
      }
      else if(mat_type==BAND_MAT_UNIT_DIAG)
      {
        if(i==j)
          a[IDX2C(i,j,lda)] = 1.0;
        else if (i-j>1) //lower bandwidth=1
          //a[IDX2C(i,j,lda)] = (double)(j + i + 1);
          a[IDX2C(i,j,lda)] = 0.0;
        else if(j-i>1) //upper bandwidth=1
          //a[IDX2C(i,j,lda)] = (double)(j + i + 1);
          a[IDX2C(i,j,lda)] = 0.0;
        else
          //a[IDX2C(i,j,lda)] = 0.0;
          a[IDX2C(i,j,lda)] = (double)(j + i + 1);
      }
      else
      {
        a[IDX2C(i,j,lda)] = (double)(j + i + 1);
      }
      //#ifdef NEGATE_OFF_DIAG
      if(offdiag_sign==NEGATE_OFF_DIAG)
      {
			     if (i != j)
				       a[IDX2C(i,j,lda)] = a[IDX2C(i,j,lda)] * (double)(-1.0);
      }
      //#ifdef BIG_DIAG
      if(mat_type==BIG_DIAG)
      {
			     if ((i + j) != 0) //skip for row=col=0
			     {
				         //implement a[x: ] = a[x: ] * (row_ind + col_ind)
				         a[IDX2C(i,j,lda)] = a[IDX2C(i,j,lda)] * (double)(j + i);
			      } //if((i+j)!=0)
      }
			//asub[(j*m) + i] = i;
		}//for (i = 0; i <m; i++)
		//xa[(j)] = j * m;
	}//for (j = 0; j<n; j++)

  //printMatrix(m, n, a, m, "dbg_print: A");
//#ifdef BIG_DIAG
  if(mat_type==BIG_DIAG)
  {
    double t1;
	  int swpind;
    swpind = lda * (n - 1);
	  for (i = 0; i <m - 1; i++) // swap larges value in the last column into diag
	  {
		    //swpind = m * (n - 1);
		    t1 = a[IDX2C(i,i,lda)]; //diag before
    //#ifdef NEGATE_OFF_DIAG
        if(offdiag_sign==NEGATE_OFF_DIAG)
        {
          a[IDX2C(i,i,lda)] = a[swpind + i] * (double)(-1.0);
  		    a[swpind + i] = (double)(t1) * (double)(-1.0);
        }
        else
        {
          a[IDX2C(i,i,lda)] = a[swpind + i];
  		    a[swpind + i] = t1;
        }
	   }
  }

	//Generate B matrix
	//#ifdef TRIMATB_LOWER_UNIT
  if(mat_type==LOWER_TRIMAT_UNIT_DIAG)
  {
    //printf("TRIMATB_LOWER_UNIT\n");
  	for (i = 0; i < m; i++) //Generate row values bcz only 1 column
  	{
  		//initialize first
  		b[i] = 1.0;
  		for (j = 0; j < i; j++) //scan all columns in that row
  		{
  			b[i] = b[i] + a[IDX2C(i,j,lda)]; //sumup all col in row i assuming x=1
  		}
  	}
  }
  //#ifdef TRIMATB_UPPER_UNIT
  else if(mat_type==UPPER_TRIMAT_UNIT_DIAG)
  {
    //printf("TRIMATB_UPPER_UNIT\n");
  	for (i = 0; i < m; i++) //Generate row values bcz only 1 column
  	{
  		//initialize first
  		b[i] = 1.0; //a[i + (i*m)];
  		for (j = i+1; j < n; j++) //scan all columns in that row
  		{
  			b[i] = b[i] + a[IDX2C(i,j,lda)]; //sumup all col in row i assuming x=1
  		}
  	}
  }
  else if(mat_type==BAND_MAT_UNIT_DIAG)
  {
    //printf("BAND_MATRIX\n");
  	for (i = 0; i < m; i++) //Generate row values bcz only 1 column
  	{
  		//initialize first
  		b[i] = 0.0; //a[i + (i*m)];
  		for (j = i-1; j <= i+1; j++) //scan all columns in that row
  		{
        if(j<0)
          continue;
        if(j>=m)
          continue;
  			b[i] = b[i] + a[IDX2C(i,j,lda)]; //sumup all col in row i assuming x=1
  		}
  	}
  }
	else
  {
    //printf("General matrix\n");
  	for (i = 0; i < m; i++) //Generate row values bcz only 1 column
  	{
  		//initialize first
  		b[i] = 0.0;
  		for (j = 0; j < n; j++) //scan all columns in that row
  		{
  			b[i] = b[i] + a[IDX2C(i,j,lda)]; //sumup all col in row i assuming x=1
  		}
  	}
  }
}
//------------------------------------------------------------------------------
void printMatrix(int m, int n, const double*A, int lda, const char* name)
{
    int row, col;
    printf("Matrix \"%s\" :-\n", name);
    for(row = 0 ; row < m ; row++){
      printf("row_%d: ", row+1);
        for(col = 0 ; col < n ; col++){
            double Areg = A[row + col*lda];
            printf("%6.2f ", Areg);
        }
      printf("\n");
    }
}
//------------------------------------------------------------------------------
int get_rows_in_range(double *A, int A_lda, int A_row_start, int A_row_end,
  int A_col_pos, double *par_A, int par_A_lda, int par_A_row_start,
  int par_A_row_end, int par_A_col_pos)
{
/* Returns the range of rows (from A_row_start to A_row_end) of column (A_col_pos)
   as part of partitioned MATRIX into par_A (from par_A_row_start to par_A_row_end)
   of column (par_A_col_pos)
double *A, Main Matrix from which the partitioned matrix will be derived
int A_lda, leading dimension of A
int A_row_start, First row from which the row ranges will be collected
int A_row_end, Last row of row ranges
int A_col_pos, Specify the column for which row ranges will be collected
double *par_A, Partiotioned Matrix pointer
int par_A_lda, leading dimension of par_A
int par_A_row_start, Put the collected row elemnts from A to par_A matrix's begining from par_A_row_start
int par_A_row_end, Put the collected row elemnts upto par_A_row_end
int par_A_col_pos, Specify the column of par_A where row ranges will be kept
*/
  int i, row, par_row;
  int ranges, par_ranges;
  ranges = A_row_end - A_row_start;
  par_ranges = par_A_row_end - par_A_row_start;
  if(par_ranges != ranges)
  {
    printf("Row ranges specified by the partitioned matrix does not matched\n");
    return(-1);
  }
  for (i = 0; i <= ranges; i++)
  {
    row = A_row_start + i;
    par_row = par_A_row_start + i;
    par_A[par_row+(par_A_col_pos * par_A_lda)] = A[row+(A_col_pos * A_lda)];
  }
}
//------------------------------------------------------------------------------
int get_partition_matrix(double *A, int A_lda, int A_row_start, int A_row_end,
  int A_col_start, int A_col_end, double *par_A, int par_A_lda,
  int par_A_row_start,int par_A_row_end, int par_A_col_start, int par_A_col_end)
{
  int i, j, col, par_col;
  int ranges, par_ranges;
  ranges = A_col_end - A_col_start;
  par_ranges = par_A_col_end - par_A_col_start;
  if(par_ranges != ranges)
  {
    printf("Column ranges specified by the partitioned matrix does not matched\n");
    return(-1);
  }
  for (i = 0; i <= ranges; i++)
  {
    col = A_col_start + i;
    par_col = par_A_col_start + i;
    get_rows_in_range(A, A_lda, A_row_start, A_row_end,
      col, par_A, par_A_lda, par_A_row_start,
      par_A_row_end, par_col);
  }
}
//------------------------------------------------------------------------------
int scale_diag(double *a, int lda, int m, int n, double *d)
{
/*scale the DIAGONAL elements of matrix "a" by the vector "d"
double *a, Matrix pointer for which the diagonal will be scaled
int lda, leading dimension of "a"
int m,  number of rows in "a"
int n,  number of column in "a" and number of rows in d vector should be equal
double *d, vector pointer for "d"
*/
  int i;
  for ( i = 0; i < n; i++) {
    a[IDX2C(i,i,lda)] = a[IDX2C(i,i,lda)] * d[i];
  }
}
//------------------------------------------------------------------------------
void mat_copy(int row, int col, int lda, double *A, double *C)
{
  /*COPY the Matrix A into Matrix C and i.e. C = A
  double *A, Source Matrix
  int lda, leading dimension of "A & C"
  int row,  number of rows in "A & C"
  int col,  number of column in "A & C"
  double *C, Destination Matrix
  */
   double t;
   int i, j;
   for (j = 0; j < col; j++)
   {
     for( i = 0; i < row; i++ )
     {
       C[i+(j*lda)] = A[i+(j*lda)];
     }
   }
}
//------------------------------------------------------------------------------
void mat_transpose(int row, int col, int lda, double *A)
{
  /*Transpose the Matrix A i.e. A = A'
  double *A, input and output matrix
  int lda, leading dimension of "A"
  int row,  number of rows in "A"
  int col,  number of column in "A"
  */
   double t;
   int i, j;
   for (i = 0; i < row; i++)
   {
     for( j = i; j < col; j++ )
     {
	      t = A[i+(j*lda)];
	      A[i+(j*lda)] = A[j+(i*lda)];
	      A[j+(i*lda)] = t;
     }
   }
}
//------------------------------------------------------------------------------
#if 0
int main(int argc, char* argv[])
{
  int dim;
  double *a, *x;
  double start, stop, req_time;
  _mat_type_ mat_type=GENERAL;
  _offdiag_sign_ offdiag_sign=DEFAULT;

  if(argc==1)
  {
    printf("\nPlease enter the Matrix size and options like this-\n");
    printf("%s n -$opt1 -$opt2\n",argv[0]);
    printf("where n is the dimnsion of matrix, i.e. matrix of n x n\n");
    printf("-$opt1 = -G will generate GENERAL matrix\n" );
    printf("-$opt1 = -L will generate LOWER TRIANGULAR matrix with UNIT DIAGONAL\n" );
    printf("-$opt1 = -U will generate UPPER TRIANGULAR matrix with UNIT DIAGONAL\n" );
    printf("-$opt1 = -B will generate BAND matrix with UNIT DIAGONAL\n" );
    printf("-$opt1 = -D will generate GENERAL matrix with BIG DIAGONAL\n" );
    printf("-$opt2 = -N will NEGATE the OFF DIAGONAL elements (optional)\n" );
    printf("e.g. %s n -B -N or %s n -B \n",argv[0],argv[0]);
    return(-100);
  }
  dim = atoi(argv[1]);
  a=(double*) malloc (dim * dim * sizeof(double) ); // host memory alloc for a
  if(a==NULL) {printf("malloc failed for a\n"); return (-2);}
  x=(double*) malloc (dim * sizeof(double) ); // host memory alloc for x
  if(x==NULL) {printf("malloc failed for x\n"); return (-2);}
  if(argc>2)
  {
    if(strcmp(argv[2],"-G")==0)
    {
      mat_type=GENERAL;
    }
    else if(strcmp(argv[2],"-L")==0)
    {
      mat_type=LOWER_TRIMAT_UNIT_DIAG;
      printf("dbg_print:1 LOWER_TRIMAT_UNIT_DIAG\n");
    }
    else if(strcmp(argv[2],"-U")==0)
    {
      mat_type=UPPER_TRIMAT_UNIT_DIAG;
      printf("dbg_print:1 UPPER_TRIMAT_UNIT_DIAG\n");
    }
    else if(strcmp(argv[2],"-B")==0)
    {
      mat_type=BAND_MAT_UNIT_DIAG;
      printf("dbg_print:1 BAND_MAT_UNIT_DIAG\n");
    }
    else if(strcmp(argv[2],"-D")==0)
    {
      mat_type=BIG_DIAG;
      printf("dbg_print:1 BIG_DIAG\n");
    }
    if(strcmp(argv[2],"-N")==0) //if default matrix option not provided but NEGATE_OFF_DIAG
    {
      offdiag_sign=NEGATE_OFF_DIAG;
    }
  }
  if(argc>3)
  {
    if(strcmp(argv[3],"-N")==0)
    {
      offdiag_sign=NEGATE_OFF_DIAG;
    }
  }

  start = mysecond();
  matgen(a, dim, dim, dim, x, mat_type, offdiag_sign);
  stop = mysecond();
  req_time = stop - start;
  printMatrix(dim, dim, a, dim, "Test A");
  printMatrix(dim, 1, x, dim, "Test X");

  //#ifdef TRIMATB_LOWER_UNIT
  if(mat_type==LOWER_TRIMAT_UNIT_DIAG)
  {
    printf("Time requires for LOWER TRIANGULAR MATRIX WITH UNIT DIAGONAL ");
    //#ifdef NEGATE_OFF_DIAG
    if(offdiag_sign==NEGATE_OFF_DIAG)
      printf("+ NEGATE_OFF_DIAG ");
    printf("%f\n", req_time);
  }
  //#ifdef TRIMATB_UPPER_UNIT
  else if(mat_type==UPPER_TRIMAT_UNIT_DIAG)
  {
    printf("Time requires for UPPER TRIANGULAR MATRIX WITH UNIT DIAGONAL ");
    //#ifdef NEGATE_OFF_DIAG
    if(offdiag_sign==NEGATE_OFF_DIAG)
      printf("+ NEGATE_OFF_DIAG ");
    printf("%f\n", req_time);
  }
  else if(mat_type==BAND_MAT_UNIT_DIAG)
  {
    printf("Time requires for BAND MATRIX WITH UNIT DIAGONAL ");
    //#ifdef NEGATE_OFF_DIAG
    if(offdiag_sign==NEGATE_OFF_DIAG)
      printf("+ NEGATE_OFF_DIAG ");
    printf("%f\n", req_time);
  }
  else
  {
    printf("Time requires for GENERAL MATRIX ");
    //#ifdef NEGATE_OFF_DIAG
    if(offdiag_sign==NEGATE_OFF_DIAG)
      printf("+ NEGATE_OFF_DIAG ");
    //#ifdef BIG_DIAG
    if(mat_type==BIG_DIAG)
      printf("+ BIG_DIAG ");
    printf("%f\n", req_time);
  }

  scale_diag(a, dim, dim, dim, x);
  printMatrix(dim, dim, a, dim, "Test A_scaled");
  //printf("%d,%f\n", dim, req_time);
  free(a);
  free(x);
}
#endif
