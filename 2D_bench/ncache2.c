# include <stdio.h>
# include <math.h>
# include <float.h>
# include <limits.h>
# include <sys/time.h>
#include <stdlib.h>

#include "matgen.h"

#ifndef N
#   define N	2000000
#endif
#ifndef NTIMES
#   define NTIMES	1
#endif
#ifndef OFFSET
#   define OFFSET	0
#endif

//# define HLINE "-------------------------------------------------------------\n"

# ifndef MIN
# define MIN(x,y) ((x)<(y)?(x):(y))
# endif
# ifndef MAX
# define MAX(x,y) ((x)>(y)?(x):(y))
# endif

#define DEBUG
/*
#ifndef L1
#define L1 (32*1024)/8
#endif

#ifndef L2
#define L2 (128*1024)/8
#endif

#ifndef L3
#define L3 (3024*1024)/8
#endif

#ifndef QUANTA
#define QUANTA 64
#endif
*/
// Data structure to set cache sizes
typedef struct s_cache
{
  int L1 ;
  int L2 ;
  int L3_u ;
  int L3_t ;
} caches;
// Data structure to set benchmarking type
typedef enum test_t
{
  TRANS, //2D Matrix Transpose
  MULT, //2D Matrix Multipliication
  INV //2D Inverse Matrix solution (Gauss Jordan Method)
} test_type;
// Data structure to hold total matrix sizes in equivalent double type floting numbers
// This is calculated based on divisor
typedef struct mat_size_s
{
  float divisor[4]; // how many percentage of cache to excercise
  int L1[4];
  int L2[4];
  int L3_u[4];
  int L3_t[4];
  int MEM[4];
} mat_size;
// Iteration numbers for different cache levels
typedef struct iteration_s
{
  int iter[4];
} looping;
//extern double mysecond();
//extern int checktick_2D(int );

int check_alphabet(char *str)
{
  // check does the string contains alphabet only
  // if there is any non alphabetic character then returns 0
  int i;
  for (i=0; str[i]!= '\0'; i++)
  {
    // check for alphabets
    if (isalpha(str[i]) == 0) // not alphabet
      return 0;
  }
  return 1;
}

int check_digit(char *str)
{
  // check does the string contains digits only
  // if there is any non digit character then returns 0
  int i;
  for (i=0; str[i]!= '\0'; i++)
  {
    // check for digits
    if (isdigit(str[i]) == 0) // not digits
      return 0;
  }
  return 1;
}

int main(int argc, char* argv[])
{
    int			quantum;
    int			BytesPerWord;
    register int i,	j, k, nc, iTIMES; //quanta_count;
    double		scalar, t;
    double		times;
    double		*a;
		double    *x;
		double    *c;
    double              avgtime;
    caches cache_size;
    test_type perf_test_type = TRANS;
    nc = 4;
    int eqv_double[4];
    cache_size.L1 = (32*1024); //32KB
    cache_size.L2 = (256*1024); //256KB
    cache_size.L3_u = (2*1024*1024); //2MB
    cache_size.L3_t = (8*1024*1024); //8MB

    FILE *fp_timing;
    FILE *fp_bw;

    if(argc==1)
    {
      printf(HLINE);
      printf("No options defined in the command line argument\n");
      printf("Please use the following command line usage\n");
      printf("%s -$tst -nc %%d -L1 %%d -L2 %%d -L3 %%d \n",argv[0] );
      printf("Where $tst is the test type. Supported test types are -\n" );
      printf("\t MM : Matrix Multipliication C = A*A \n");
      printf("\t MT : Matrix Transpose A = trans(A) \n");
      printf("\t MS : Matrix Solving using Gauss-Jordan Elimination if AX = B then X = INV(A)*B \n");
      printf(" -nc %%d provides number of cores in the processor. e.g. for defining 4 cores plz use -nc 4\n");
      printf(" -L1 %%d -L2 %%d -L3 %%d  provides the L1, L2, L3 sizes in KB. e.g. for defining 32KB L1 cache plz use -L1 32\n");
      printf(HLINE);
      exit(-1);
    }
    printf(HLINE);
    i = argc - 1;
    while(i!=0)
    {
      if(strcmp(argv[i],"-MM")==0)
      {
        perf_test_type = MULT;
        i--;
      }
      else if(strcmp(argv[i],"-MT")==0)
      {
        perf_test_type = TRANS;
        i--;
      }
      else if(strcmp(argv[i],"-MS")==0)
      {
        perf_test_type = INV;
        i--;
      }
      else if(strcmp(argv[i],"-nc")==0)
      {
        if(argc >= i+2)
        {
          if(check_digit(argv[i+1]))
          {
            nc = atoi(argv[i+1]);
            if(nc<=0)
            {
              printf("The number specified with -nc can not be <= 0 [you given as -nc %d]\n", nc);
              exit(-1);
            }
          }
          else
          {
            printf("The number specified with -nc is not a valid digit/number\n");
            exit(-1);
          }
        }
        else
        {
          printf(":ERROR: number of core is not given with option -nc \n");
          exit(-1);
        }
        i--;
      }
      else if(strcmp(argv[i],"-L1")==0)
      {
        if(argc >= i+2)
        {
          if(check_digit(argv[i+1]))
          {
            cache_size.L1 = atoi(argv[i+1]);
            if(cache_size.L1<=0)
            {
              printf("The number specified with -L1 can not be <= 0 [you given as -L1 %d]\n", cache_size.L1);
              exit(-1);
            }
            cache_size.L1 = cache_size.L1 * 1024;
          }
          else
          {
            printf("The number specified with -L1 is not a valid digit/number\n");
            exit(-1);
          }
        }
        else
        {
          printf(":ERROR: L1 size is not defined with -L1 \n");
          exit(-1);
        }
        i--;
      }
      else if(strcmp(argv[i],"-L2")==0)
      {
        if(argc >= i+2)
        {
          if(check_digit(argv[i+1]))
          {
            cache_size.L2 = atoi(argv[i+1]);
            if(cache_size.L2<=0)
            {
              printf("The number specified with -L2 can not be <= 0 [you given as -L2 %d]\n", cache_size.L2);
              exit(-1);
            }
            cache_size.L2 = cache_size.L2 * 1024;
          }
          else
          {
            printf("The number specified with -L2 is not a valid digit/number\n");
            exit(-1);
          }
        }
        else
        {
          printf(":ERROR: L2 size is not defined with -L2 \n");
          exit(-1);
        }
        i--;
      }
      else if(strcmp(argv[i],"-L3")==0)
      {
        if(argc >= i+2)
        {
          if(check_digit(argv[i+1]))
          {
            cache_size.L3_t = atoi(argv[i+1]);
            if(cache_size.L3_t<=0)
            {
              printf("The number specified with -L3 can not be <= 0 [you given as -L3 %d]\n", cache_size.L3_t);
              exit(-1);
            }
            cache_size.L3_t = cache_size.L3_t * 1024;
          }
          else
          {
            printf("The number specified with -L3 is not a valid digit/number\n");
            exit(-1);
          }
        }
        else
        {
          printf(":ERROR: L3 size is not defined with -L3 \n");
          exit(-1);
        }
        i--;
      }
      else
      {
        i--;
      }
    }
    cache_size.L3_u = cache_size.L3_t / nc;
    BytesPerWord = sizeof(double);
    // Equivalent double precision floting point numbers
    eqv_double[0] = cache_size.L1 / BytesPerWord;
    eqv_double[1] = cache_size.L2 / BytesPerWord;
    eqv_double[2] = cache_size.L3_u / BytesPerWord;
    eqv_double[3] = cache_size.L3_t / BytesPerWord;

    printf("Starting Benchmark of ");
    if(perf_test_type==MULT)
      printf("Matrix Maultiplication");
    else if(perf_test_type==INV)
      printf("Inverse Matrix Solve");
    else
      printf("Matrix Transpose");
    printf(" with %d bytes per DOUBLE PRECISION word.\n", BytesPerWord);
    printf("L1 Cache Size = %d byte and equivalnt %d double precision floating-point numbers\n", cache_size.L1, eqv_double[0]);
    printf("L2 Cache Size = %d byte and equivalnt %d double precision floating-point numbers\n", cache_size.L2, eqv_double[1]);
    printf("L3 Cache Size = %d byte and equivalnt %d double precision floating-point numbers\n", cache_size.L3_t, eqv_double[3]);

    //printf("Each test will run %d times\n", NTIMES);
    printf(HLINE);
    fp_timing = fopen("./timing.rpt","w");
    if(fp_timing==NULL)
    {
      printf("Error opening file 'timing.rpt'\n");
      return(-1);
    }
    fp_bw = fopen("./bw.rpt","w");
    if(fp_bw==NULL)
    {
      printf("Error opening file 'bw.rpt'\n");
      return(-2);
    }

    mat_size quanta_count;
    quanta_count.divisor[0] = 0.125; //12.5%
    quanta_count.divisor[1] = 0.25;  //25%
    quanta_count.divisor[2] = 0.50;  //50%
    quanta_count.divisor[3] = 0.75;  //75%
    for(i=0; i<4; i++)
    {
      quanta_count.L1[i] = (int)((float)(eqv_double[0]) * quanta_count.divisor[i]);
      quanta_count.L2[i] = (int)((float)(eqv_double[1]) * quanta_count.divisor[i]) + eqv_double[0];
      quanta_count.L3_u[i] = (int)((float)(eqv_double[2]) * quanta_count.divisor[i]) + eqv_double[0] + eqv_double[1];
      quanta_count.L3_t[i] = (int)((float)(eqv_double[3]) * quanta_count.divisor[i]) + eqv_double[0] + eqv_double[1];
#ifdef DEBUG
      printf("quanta_count.L1[%d] = %d\n", i, quanta_count.L1[i]);
      printf("quanta_count.L2[%d] = %d\n", i, quanta_count.L2[i]);
      printf("quanta_count.L3_u[%d] = %d\n", i, quanta_count.L3_u[i]);
      printf("quanta_count.L3_t[%d] = %d\n", i, quanta_count.L3_t[i]);
#endif
    }
    int test_arr_sz[16];
    double temp;
    for(i=0; i<4; i++)
    {
      if(perf_test_type==MULT || perf_test_type==INV)
      {
        temp = sqrt((float)quanta_count.L1[i] / 2);
        test_arr_sz[i] = (int) temp;
#ifdef DEBUG
        int loss = quanta_count.L1[i] - (2 * test_arr_sz[i] * test_arr_sz[i]) ;
        printf("byte loss for test_arr_sz[%d] = %d\n",i,loss*8);
#endif
      }
      else
      {
        temp = sqrt((float)quanta_count.L1[i]);
        test_arr_sz[i] = (int) temp;
#ifdef DEBUG
        int loss = quanta_count.L1[i] - (test_arr_sz[i] * test_arr_sz[i]) ;
        printf("byte loss for test_arr_sz[%d] = %d\n",i,loss*8);
#endif
      }
    }
    for(i=0; i<4; i++)
    {
      if(perf_test_type==MULT || perf_test_type==INV)
      {
        temp = sqrt((float)quanta_count.L2[i] / 2);
        test_arr_sz[i+4] = (int) temp;
#ifdef DEBUG
        int loss = quanta_count.L2[i] - (2 * test_arr_sz[i+4] * test_arr_sz[i+4]);
        printf("byte loss for test_arr_sz[%d] = %d\n",i+4,loss*8);
#endif
      }
      else
      {
        temp = sqrt((float)quanta_count.L2[i]);
        test_arr_sz[i+4] = (int) temp;
#ifdef DEBUG
        int loss = quanta_count.L2[i] - ( test_arr_sz[i+4] * test_arr_sz[i+4]);
        printf("byte loss for test_arr_sz[%d] = %d\n",i+4,loss*8);
#endif
      }
    }
    for(i=0; i<4; i++)
    {
      if(perf_test_type==MULT || perf_test_type==INV)
      {
        temp = sqrt((float)quanta_count.L3_u[i] / 2);
        test_arr_sz[i+8] = (int) temp;
#ifdef DEBUG
        int loss = quanta_count.L3_u[i] - (2 * test_arr_sz[i+8] * test_arr_sz[i+8]);
        printf("byte loss for test_arr_sz[%d] = %d\n",i+8,loss*8);
#endif
      }
      else
      {
        temp = sqrt((float)quanta_count.L3_u[i]);
        test_arr_sz[i+8] = (int) temp;
#ifdef DEBUG
        int loss = quanta_count.L3_u[i] - (test_arr_sz[i+8] * test_arr_sz[i+8]);
        printf("byte loss for test_arr_sz[%d] = %d\n",i+8,loss*8);
#endif
      }
    }
    for(i=0; i<4; i++)
    {
      if(perf_test_type==MULT || perf_test_type==INV)
      {
        temp = sqrt((float)quanta_count.L3_t[i] / 2);
        test_arr_sz[i+12] = (int) temp;
#ifdef DEBUG
        int loss = quanta_count.L3_t[i] - (2 * test_arr_sz[i+12] * test_arr_sz[i+12]);
        printf("byte loss for test_arr_sz[%d] = %d\n",i+12,loss*8);
#endif
      }
      else
      {
        temp = sqrt((float)quanta_count.L3_t[i]);
        test_arr_sz[i+12] = (int) temp;
#ifdef DEBUG
        int loss = quanta_count.L3_t[i] - (test_arr_sz[i+12] * test_arr_sz[i+12]);
        printf("byte loss for test_arr_sz[%d] = %d\n",i+12,loss*8);
#endif
      }
    }
    for(i=0; i<16; i++)
    {
      printf("test_arr_sz[%d] = %d \n",i,test_arr_sz[i]);
    }
    looping loop_var;
    if(perf_test_type==MULT || perf_test_type==INV)
    {
      loop_var.iter[0] = 200;
      loop_var.iter[1] = 70;
      loop_var.iter[2] = 70;
      loop_var.iter[3] = 50;
    }
    else
    {
      loop_var.iter[0] = 2500;
      loop_var.iter[1] = 2500;
      loop_var.iter[2] = 2500;
      loop_var.iter[3] = 2500;
    }
//#if 0
    for(i = 0; i<16; i++)
    {
      quantum = test_arr_sz[i];
       if(!(a = malloc( sizeof(double)*(quantum*quantum) )) )
       {
          printf("malloc failed for 'a' at size:%d \n",quantum);
          return(-1);
       }
			 x=(double*) malloc (quantum * sizeof(double) ); // host memory alloc for x
		   if(x==NULL) {printf("malloc failed for x\n"); return (-2);}
       if(perf_test_type==MULT||perf_test_type==INV)
       {
         c=(double*) malloc (quantum * quantum * sizeof(double) ); // host memory alloc for c
  		   if(c==NULL) {printf("malloc failed for c\n"); return (-3);}
       }
		   double alpha = 1.0;
		   double beta = 0.0;
       // initialize the test matrix
       matgen(a, quantum, quantum, quantum, x, GENERAL, NEGATE_OFF_DIAG);
       iTIMES = loop_var.iter[i/4]; //checktick_2D(quantum);
       printf("Matrix %d x %d with iTIMES = %d\n",quantum,quantum,iTIMES);
       //--printMatrix(quantum, quantum, a, quantum, "test matrix");
       // repeat test cases iTIMES times
       times = mysecond();
       for (k=0; k<iTIMES; k++)
       {
          if(k==1)
          {
            times = mysecond();
          }
          if(perf_test_type==MULT)
          {
            int status = dgemm_("N", "N", &quantum, &quantum, &quantum, &alpha, a, &quantum, a, &quantum, &beta, c, &quantum);
          }
          else if(perf_test_type==INV)
          {
            mat_copy(quantum, quantum, quantum, a, c);
            int status = GJ_Inv_slv(quantum, quantum, c, x);
          }
          else
          {
            mat_transpose(quantum, quantum, quantum, a);
          }
       }
       times = mysecond() - times;
       //	--- SUMMARY ---
       avgtime = times / (double)(iTIMES-1);
       double calc; //Calculated numbr of bytes having memory R/W
       if(perf_test_type==MULT)
       {
         calc = pow((double)quantum, 3); //n^3
         calc = calc * 2; // 2*n^3
         calc = calc + pow((double)quantum, 2); // 2*n^3 + n^2
         calc =  calc * 8; //(2*n^3 + n^2)*8
       }
       else if(perf_test_type==INV)
       {
         double c_times, kc_times, l_times;
         c_times = (double)(quantum*(quantum+1));
         c_times = c_times/2;
         l_times = (double)(quantum*(quantum-1));
         l_times = l_times/2;
         kc_times = pow((double)quantum, 3); //n^3
         kc_times = kc_times - (double)quantum; //n^3 -n
         kc_times = kc_times/3; //(n^3 -n)/3
         calc = (double)quantum * 8; //8n
         calc = calc + (c_times*2) + (l_times*7) + (kc_times*3);
         calc = calc + pow((double)quantum, 2);
         calc =  calc * 8;
       }
       else
       {
         calc = pow((double)quantum, 2); //n^2
         calc = calc * 4; // 4*n^2
         calc =  calc * 8; //(4*n^2)*8
       }
       double bw = (calc / avgtime) / 1.0e9;
       if(perf_test_type==MULT||perf_test_type==INV)
       {
         fprintf(fp_timing, "%d, %.2f, %.2fKB\n", 2*quantum*quantum*8/1024, avgtime*1e6, calc/1024);
         fprintf(fp_bw, "%d, %.2f, %.2fKB\n", 2*quantum*quantum*8/1024, bw, calc/1024);
       }
       else
       {
         fprintf(fp_timing, "%d, %.2f, %.2fKB\n", quantum*quantum*8/1024, avgtime*1e6, calc/1024);
         fprintf(fp_bw, "%d, %.2f, %.2fKB\n", quantum*quantum*8/1024, bw, calc/1024);
       }
       free(a);
       free(x);
       if(perf_test_type==MULT||perf_test_type==INV)
       {
         free(c);
       }
    }
//#endif
    fclose(fp_timing);
    fclose(fp_bw);
    printf("\n");

}

# define	M	20
int checktick_2D(int varN)
{
    int		i, minDelta, Delta;
    double	t1, t2, timesfound[M];
    int j, x, y;
    double *ta;
//    varN = 32;
    if( !(ta = malloc(sizeof(double)*varN*varN))) printf("malloc failed\n");

/*  Collect a sequence of M unique time values from the system. */
    x = NTIMES;
    for (i = 0; i < M; i++) {
        loop:
	t1 = mysecond();
	for(j=0; j<x; j++)
        {
             for(y=0; y<varN*varN; y++)
                  ta[y] = (float) (y);
        }
	t2 = mysecond();
	timesfound[i] = t2 - t1;
        if(fabs(timesfound[i])<20.0E-6)
        {
		x = x + NTIMES;
		goto loop;
        }
    }

#if 0
    printf("Suggested looping (x) = %d\n",x);
    for (i = 0; i < M; i++) {
        printf("timesfound[%d] = %f\n",i,timesfound[i]);
    }
#endif

   return(x);
}
