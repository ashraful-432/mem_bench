
//#define TRIMATB_LOWER_UNIT
//#define TRIMATB_UPPER_UNIT
//#define NEGATE_OFF_DIAG
//#define BIG_DIAG
typedef enum {
  GENERAL,
  LOWER_TRIMAT_UNIT_DIAG,
  UPPER_TRIMAT_UNIT_DIAG,
  BAND_MAT_UNIT_DIAG,
  BIG_DIAG
} _mat_type_;
//_mat_type_ mat_type=DEFAULT;
typedef enum {
  DEFAULT,
  NEGATE_OFF_DIAG
} _offdiag_sign_;
//_offdiag_sign_ offdiag_sign=DEFAULT;
#define IDX2C(i,j,ld) (( i )+(( j )*( ld ))) //i:row, j:column, lda:leading dimension
#define HLINE "----------------"
#ifdef __cpp
extern "C" {
#endif
  double inline mysecond();
  void inline matgen(double *, int , int , int , double *, _mat_type_ , _offdiag_sign_ );
  void printMatrix(int , int , const double *, int , const char* );
  int get_rows_in_range(double *, int , int , int ,
    int , double *, int , int ,
    int , int );
  int get_partition_matrix(double *, int , int , int ,
      int , int , double *, int ,
      int ,int , int , int );
#ifdef __cpp
}
#endif
