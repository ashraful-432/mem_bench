#include <stdio.h>
#include <math.h>
#include <sys/time.h>
#include <stdlib.h>

#include "matgen.h"
//#include "blaswrap.h"
//#include "f2c.h"

/* Subroutine */
int dgemm_(char *transa, char *transb, int *m, int *n,
  int *k, double *alpha, double *a, int *lda,
	double *b, int *ldb, double *beta, double *c__,
	int *ldc);

int main(int argc, char* argv[])
{
  int dim;
  double *a, *x;
  double start, stop, req_time;
  _mat_type_ mat_type=GENERAL;
  _offdiag_sign_ offdiag_sign=DEFAULT;

  if(argc==1)
  {
    printf("\nPlease enter the Matrix size and options like this-\n");
    printf("%s n -$opt1 -$opt2\n",argv[0]);
    printf("where n is the dimnsion of matrix, i.e. matrix of n x n\n");
    printf("-$opt1 = -G will generate GENERAL matrix\n" );
    printf("-$opt1 = -L will generate LOWER TRIANGULAR matrix with UNIT DIAGONAL\n" );
    printf("-$opt1 = -U will generate UPPER TRIANGULAR matrix with UNIT DIAGONAL\n" );
    printf("-$opt1 = -B will generate BAND matrix with UNIT DIAGONAL\n" );
    printf("-$opt1 = -D will generate GENERAL matrix with BIG DIAGONAL\n" );
    printf("-$opt2 = -N will NEGATE the OFF DIAGONAL elements (optional)\n" );
    printf("e.g. %s n -B -N or %s n -B \n",argv[0],argv[0]);
    return(-100);
  }
  dim = atoi(argv[1]);
  a=(double*) malloc (dim * dim * sizeof(double) ); // host memory alloc for a
  if(a==NULL) {printf("malloc failed for a\n"); return (-2);}
  x=(double*) malloc (dim * sizeof(double) ); // host memory alloc for x
  if(x==NULL) {printf("malloc failed for x\n"); return (-2);}
  if(argc>2)
  {
    if(strcmp(argv[2],"-G")==0)
    {
      mat_type=GENERAL;
    }
    else if(strcmp(argv[2],"-L")==0)
    {
      mat_type=LOWER_TRIMAT_UNIT_DIAG;
      printf("dbg_print:1 LOWER_TRIMAT_UNIT_DIAG\n");
    }
    else if(strcmp(argv[2],"-U")==0)
    {
      mat_type=UPPER_TRIMAT_UNIT_DIAG;
      printf("dbg_print:1 UPPER_TRIMAT_UNIT_DIAG\n");
    }
    else if(strcmp(argv[2],"-B")==0)
    {
      mat_type=BAND_MAT_UNIT_DIAG;
      printf("dbg_print:1 BAND_MAT_UNIT_DIAG\n");
    }
    else if(strcmp(argv[2],"-D")==0)
    {
      mat_type=BIG_DIAG;
      printf("dbg_print:1 BIG_DIAG\n");
    }
    if(strcmp(argv[2],"-N")==0) //if default matrix option not provided but NEGATE_OFF_DIAG
    {
      offdiag_sign=NEGATE_OFF_DIAG;
    }
  }
  if(argc>3)
  {
    if(strcmp(argv[3],"-N")==0)
    {
      offdiag_sign=NEGATE_OFF_DIAG;
    }
  }

  matgen(a, dim, dim, dim, x, mat_type, offdiag_sign);
  printMatrix(dim, dim, a, dim, "Test A");
  double *c;
  c=(double*) malloc (dim * dim * sizeof(double) ); // host memory alloc for c
  if(c==NULL) {printf("malloc failed for c\n"); return (-3);}
  double alpha = 1.0;
  double beta = 0.0;
  //int status = dgemm_('N', 'N', &dim, &dim, &dim, &alpha, a, &dim, a, &dim, &beta, c, &dim);
  int status = dgemm_("N", "N", &dim, &dim, &dim, &alpha, a, &dim, a, &dim, &beta, c, &dim);
  printf("returned status = %d\n",status);
  printMatrix(dim, dim, c, dim, "Test C");
}
