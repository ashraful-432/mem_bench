# include <stdio.h>
# include <math.h>
# include <float.h>
# include <limits.h>
# include <sys/time.h>
#include <stdlib.h>

#include "matgen.h"

#ifndef N
#   define N	2000000
#endif
#ifndef NTIMES
#   define NTIMES	50
#endif
#ifndef OFFSET
#   define OFFSET	0
#endif

//# define HLINE "-------------------------------------------------------------\n"

# ifndef MIN
# define MIN(x,y) ((x)<(y)?(x):(y))
# endif
# ifndef MAX
# define MAX(x,y) ((x)>(y)?(x):(y))
# endif

#ifndef L1
#define L1 (32*1024)/8
#endif

#ifndef L2
#define L2 (128*1024)/8
#endif

#ifndef L3
#define L3 (3024*1024)/8
#endif


#ifndef QUANTA
#define QUANTA 16
#endif

//extern double mysecond();
//extern int checktick_2D(int );


int main()
{
    int			quantum;
    int			BytesPerWord;
    register int	j, k, iTIMES, quanta_count;
    double		scalar, t;
    double		times;
    double		*a;
		double    *x;
		double    *c;
    double              avgtime;

    FILE *fp_timing;
    FILE *fp_bw;

    fp_timing = fopen("./timing.rpt","w");
    if(fp_timing==NULL)
    {
      printf("Error opening file 'timing.rpt'\n");
      return(-1);
    }
    fp_bw = fopen("./bw.rpt","w");
    if(fp_bw==NULL)
    {
      printf("Error opening file 'bw.rpt'\n");
      return(-2);
    }

    printf(HLINE);
    BytesPerWord = sizeof(double);
    printf("Starting Benchmark with %d bytes per DOUBLE PRECISION word.\n",
	BytesPerWord);
    printf("L1 Cache Size = %d double precision floating-point numbers\n", L1);
    printf("L2 Cache Size = %d double precision floating-point numbers\n", L2);
    printf("L3 Cache Size = %d double precision floating-point numbers\n", L3);

    //printf("Each test will run %d times\n", NTIMES);
    printf(HLINE);
    quantum = QUANTA;
    quanta_count=1;

    while((quantum*quantum)<L3)
    {
       if(!(a = malloc( sizeof(double)*(quantum*quantum) )) )
       {
          printf("malloc failed for 'a' at size:%d \n",quantum);
          return(-1);
       }
			 x=(double*) malloc (quantum * sizeof(double) ); // host memory alloc for x
		   if(x==NULL) {printf("malloc failed for x\n"); return (-2);}
		   c=(double*) malloc (quantum * quantum * sizeof(double) ); // host memory alloc for c
		   if(c==NULL) {printf("malloc failed for c\n"); return (-3);}
		   double alpha = 1.0;
		   double beta = 0.0;
       // initialize the test matrix
       matgen(a, quantum, quantum, quantum, x, GENERAL, NEGATE_OFF_DIAG);
       iTIMES = NTIMES; //checktick_2D(quantum);
       //printf("Matrix %d x %d with iTIMES = %d\n",quantum,quantum,iTIMES);
       //--printMatrix(quantum, quantum, a, quantum, "test matrix");
       // repeat test cases iTIMES times
       times = mysecond();
       for (k=0; k<iTIMES; k++)
       {
          if(k==1)
            times = mysecond();
					//mat_transpose(quantum, quantum, quantum, a);
					int status = dgemm_("N", "N", &quantum, &quantum, &quantum, &alpha, a, &quantum, a, &quantum, &beta, c, &quantum);
       }
       times = mysecond() - times;
       //	--- SUMMARY ---
       avgtime = times / (double)(iTIMES-1);
       fprintf(fp_timing, "%d, %5.4f\n", quantum, avgtime*1e6);
       fprintf(fp_bw, "%d, %.4f\n",quantum, ((quantum*quantum*8) / avgtime)/1e6);
       free(a);
			 free(x);
			 free(c);
       quanta_count++;
       if(quantum>L1)
       {
          quantum = quantum + (QUANTA);
       }
       /*else if(quantum>L1)
       {
          quantum = quantum + (2*QUANTA);
       }*/
       else
       {
          quantum = quantum + (0.5*QUANTA);
       }
    }
    fclose(fp_timing);
    fclose(fp_bw);
}

# define	M	20
int checktick_2D(int varN)
{
    int		i, minDelta, Delta;
    double	t1, t2, timesfound[M];
    int j, x, y;
    double *ta;
//    varN = 32;
    if( !(ta = malloc(sizeof(double)*varN*varN))) printf("malloc failed\n");

/*  Collect a sequence of M unique time values from the system. */
    x = NTIMES;
    for (i = 0; i < M; i++) {
        loop:
	t1 = mysecond();
	for(j=0; j<x; j++)
        {
             for(y=0; y<varN*varN; y++)
                  ta[y] = (float) (y);
        }
	t2 = mysecond();
	timesfound[i] = t2 - t1;
        if(fabs(timesfound[i])<20.0E-6)
        {
		x = x + NTIMES;
		goto loop;
        }
    }

#if 0
    printf("Suggested looping (x) = %d\n",x);
    for (i = 0; i < M; i++) {
        printf("timesfound[%d] = %f\n",i,timesfound[i]);
    }
#endif

   return(x);
}
